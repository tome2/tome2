#pragma once

/**
 * Player sex descriptor.
 */
struct player_sex
{
	/**
	 * Type of sex.
	 */
	char const *title;

	/**
	 * Winner title.
	 */
	char const *winner;
};
