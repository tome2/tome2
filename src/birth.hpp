#pragma once

#include "h-basic.h"

extern void print_desc_aux(cptr txt, int y, int x);
extern void save_savefile_names(void);
extern bool_ begin_screen(void);
extern void get_height_weight(void);
extern void player_birth(void);
